
from typing import Type
import uuid
from django.http import response
from django.utils import timezone
import jwt
from webapi.auth.permission import IsAuth, IsUser
from webapi.serializer.user_serializer import UserSerializer
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes

from rest_framework.request import HttpRequest, Request
from django.http.response import HttpResponse, JsonResponse
from rest_framework import status
from webapi.serializer.post_serializer import PostSerializer
from webapi.view_model.postVMs import  PostModel

@api_view(['POST', 'PUT' , 'GET'])
@permission_classes([IsAuth, IsUser])
def post_api(request: HttpRequest):
    
    if request.method == 'POST':
        postSerializer = PostSerializer()
        token = request.headers.get('Authorization')
        userId = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('userId')
        time = timezone.now()
        data = PostModel(
            description= request.data.get("description"),
            postId= uuid.uuid4().hex[:50],
            timeCreated= time,
            totalComment=0,
            totalLike=0,
            userId = None,
            postImage=request.data.get("postImages")
        )
        check = postSerializer.create(data = data, userId= userId)
        if check:
            return JsonResponse({"message": "Post success"}, status = status.HTTP_201_CREATED)
        else:
            return JsonResponse({"message":"Post fail"}, status = status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        data = PostModel(
            description= request.data.get("description"),
            postId= request.data.get("postId"),
            timeCreated= None,
            totalComment=0,
            totalLike=0,
            userId = None
        )
        postSerializer = PostSerializer()
        check = postSerializer.update(data = data)
        if check:
            # return JsonResponse({"message": "Update success"} , status = status.HTTP_200_OK)
            return response(data = {"message": "Update success"},status = status.HTTP_200_OK, headers = {'Access-Control-Allow-Origin': 'http://localhost:3000/','Access-Control-Allow-Origin':'*'})
        else:
            return JsonResponse({"message":"Update fail"}, status = status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        postSerializer = PostSerializer()
        result = postSerializer.get_post(request.GET.get("userId"))
        return JsonResponse({"message": result}, status = status.HTTP_200_OK)
    else:
        return JsonResponse({"message":"not found"}, status = status.HTTP_404_NOT_FOUND)    

@api_view(['POST'])
@permission_classes([IsAuth, IsUser])
def like_post(request: HttpRequest):
    token = request.headers.get('Authorization')
    userId = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('userId')
    postId = request.GET.get("postId")
    postSerializer = PostSerializer()
    if postSerializer.like_post(postId= postId, userId= userId):
        return JsonResponse({"message": "like post success"}, status = status.HTTP_200_OK)
    else:
        return JsonResponse({"message":"like post fail"}, status = status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuth, IsUser])
def comment_post(request: HttpRequest):
    token = request.headers.get('Authorization')
    userId = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('userId')
    postId = request.GET.get("postId")
    postSerializer = PostSerializer()
    if postSerializer.comment_post(postId= postId, userId= userId, data= request.data):
        return JsonResponse({"message": "comment post success"}, status = status.HTTP_200_OK)
    else:
        return JsonResponse({"message":"comment post fail"}, status = status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@permission_classes([IsAuth, IsUser])
def get_post_for_user(request: Request):
    token = request.headers.get('Authorization')
    userId = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('userId')
    page = request.query_params.get("page")
    print(page)
    postSerializer = PostSerializer()
    return JsonResponse({"data": postSerializer.get_post_for_user(userId=userId,page=int(page))}, status = status.HTTP_200_OK)
