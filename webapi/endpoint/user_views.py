
import jwt
from webapi.auth.permission import IsAuth, IsUser
from webapi.serializer.user_serializer import UserSerializer
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes

from rest_framework.request import HttpRequest, Request
from django.http.response import JsonResponse
from rest_framework import status


@api_view(['POST'])
def register(request: Request):

    data = request.data
    userModel = UserCreateModel(
        username=data.get("username"),
        password=data.get("password"),
        avatar=data.get("avatar"),
        email=data.get("email"),
        fullname=data.get("fullname"),
    )

    userSerializer = UserSerializer()

    userSerializer.create(userModel)

    return JsonResponse({"message": "success"}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def login(request: Request):

    # data =  request.data
    userLoginModel = UserLoginModel(
        username=request.data.get("username"),
        password=request.data.get("password")
    )

    userSerializer = UserSerializer()

    token = userSerializer.login(userLoginModel)

    if token:
        return JsonResponse({"token": token}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({"message": "Invalid username or password"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuth])
def fetch_user(request: HttpRequest):
    bearer_token = request.headers.get("Authorization")
    if bearer_token:
        token = bearer_token.split(" ").__getitem__(1)
        if token:
            userSerializer = UserSerializer()
            result = userSerializer.fetch_user(token)
            if result:
                return JsonResponse(result, status=status.HTTP_200_OK, safe=False)
            else:
                return JsonResponse({"message": "Invalid token"}, status=status.HTTP_403_FORBIDDEN)
        else:
            JsonResponse({"message": "token not found"},
                         status=status.HTTP_401_UNAUTHORIZED)
    else:
        JsonResponse({"message": "token not found"},
                     status=status.HTTP_401_UNAUTHORIZED)


@api_view(['POST'])
@permission_classes([IsAuth, IsUser])
def request_friend(request: Request):
    userId = request.data.get("userId")
    friendId = request.data.get("friendId")

    if userId != None and friendId != None:
        userSerializer = UserSerializer()
        check = userSerializer.request_friend(userId=userId, friendId=friendId)
        if check:
            return JsonResponse({"message": "Request sent success"}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"message": "Request sent fail"}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return JsonResponse({"message": "Request sent fail"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuth, IsUser])
def get_friend_request(request: HttpRequest):
    userSerializer = UserSerializer()
    result = userSerializer.get_friend_request(
        token=request.headers.get("token"))
    return JsonResponse({"result": result}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuth, IsUser])
def accept_request(request: Request):
    relationId = request.data.get("relationId")
    userSerializer = UserSerializer()
    check = userSerializer.accept_request(relationId=relationId)
    if check:
        return JsonResponse({"message": "Accept request success"}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({"message": "Accept request fail"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuth, IsUser])
def get_recommend(request: HttpRequest):
    token = request.headers.get('Authorization')
    userId = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('userId')
    userSerializer = UserSerializer()
    result = userSerializer.get_recommend_friend(userId=userId)
    return JsonResponse({"data":result}, status=status.HTTP_200_OK)
