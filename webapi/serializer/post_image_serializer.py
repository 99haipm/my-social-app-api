from typing import List
from webapi.view_model.postVMs import  PostModel
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models, transaction
from django.db.models.base import Model
from rest_framework import fields, serializers 
from webapi.models import Post, PostImage, User,Role, UserFriend
import uuid 
from django.contrib.auth.hashers import make_password, check_password
import jwt


class PostImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostImage
        fields = ('imageId','imageUrl','status', 'postId')
    
    def create(self, validated_data):

        return super().create(validated_data)
    def createMultipleImage(self, images: List[str], post: Post):
        try:
            with transaction.atomic():
                for item in images:
                    new_image = PostImage(
                        imageId = uuid.uuid4().hex[:50],
                        imageUrl = item,
                        status = "Active",
                        postId = post,
                    )
                    dict =  new_image.__dict__
                    dict.pop("_state")
                    super().create(validated_data= new_image.__dict__)
                return True
        except Exception as e:
            print(e)
            transaction.rollback()
            return False
        