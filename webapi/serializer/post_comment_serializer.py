from typing import List
from webapi.view_model.postVMs import  PostCommentCreateModel, PostModel
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models, transaction
from django.db.models.base import Model
from rest_framework import fields, serializers 
from webapi.models import Post, PostComment, PostImage, User,Role, UserFriend
import uuid 
from django.contrib.auth.hashers import make_password, check_password
import jwt



class PostCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostComment
        fields = ('commentId', 'sequenceNum', 'content', 'userId', 'timeCreated', 'status', 'postId')
    
    def create(self, data:PostCommentCreateModel):
        validated_data = data.__dict__
        if super().create(validated_data):
            return True
        else:
            return False