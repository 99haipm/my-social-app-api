
from typing import List
from webapi.serializer.post_comment_serializer import PostCommentSerializer
from webapi.serializer.post_like_serializer import PostLikeSerializer
from webapi.serializer.post_image_serializer import PostImageSerializer
from webapi.view_model.postVMs import  PostCommentCreateModel, PostGeneralModel, PostModel
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models, transaction
from django.db.models.base import Model
from rest_framework import fields, serializers 
from webapi.models import Post, PostComment, PostImage, PostLike, User,Role, UserFriend
import uuid 
from django.contrib.auth.hashers import make_password, check_password
import jwt
from django.utils import timezone

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('postId', 'description', 'totalLike','totalComment','timeCreated','userId')
    
    def create(self, data : PostModel, userId: str):
        try:
            user_instance = User.objects.get(userId = userId)
            data.userId = user_instance
            validated_data = data.__dict__
            print(validated_data.get("postImage"))
            if validated_data.get("postImage") != None:
                images = validated_data.get("postImage")
                print(images)
                validated_data.pop("postImage")
            with transaction.atomic():
                newPost = super().create(validated_data)
                if newPost:
                    if images != []:
                        postImageSerializer = PostImageSerializer()
                        postImageSerializer.createMultipleImage(images,newPost)
                    return True
                else:
                    return False
        except Exception as e:
            print(e)
            return False
    
    def update(self, data:PostModel):
        try:
            post_instance = Post.objects.get(postId= data.postId)
            validatedData = post_instance
            validatedData.description = data.description
            return super().update(post_instance, validatedData.__dict__)
        except Exception as e:
            print (e)
            return False
    def get_post(self, userId: str):
        try:
            result = list()
            images = list()
            currentUser = User.objects.get(userId = userId)
            for item in Post.objects.filter(userId = userId):
                post = PostGeneralModel(
                    postId=item.postId,
                    description= item.description,
                    postImage=[],
                    timeCreated= item.timeCreated,
                    totalComment= item.totalComment,
                    totalLike= item.totalLike,
                    userId= item.userId.userId,
                    postLike=[]
                )

                for image in PostImage.objects.filter(postId = post.postId):
                    image_dict = image.__dict__
                    image_dict.pop("_state")
                    post.postImage.append(image_dict)

                for like in PostLike.objects.filter(postId = post.postId):
                    like_item = {
                        "username" : like.userId.username,
                        "fullname" : like.userId.fullname
                    }
                    post.postLike.append(like_item)
                result.append(post.__dict__)
            return sorted(result, key=lambda x: x["timeCreated"], reverse=True)
        except Exception as e:
            print(e)
            return []
    def like_post(self, postId:str,userId:str):
        try:
            with transaction.atomic():
                post = Post.objects.get(postId = postId)
                user = User.objects.get(userId = userId)
                validated_data = post
                validated_data.totalLike = validated_data.totalLike + 1
                inst = super().update(instance= post,validated_data=validated_data.__dict__)
                if inst:
                    postLikeSerializer = PostLikeSerializer()
                    if postLikeSerializer.create(inst, user):
                        return True
                    else:
                        transaction.rollback()
                        return False
                else:
                    transaction.rollback()
                    return False

        except Exception as e:
            transaction.rollback()
            print(e)
            return False
    
    def comment_post(self, postId:str, userId:str, data: dict[str,any]):
        postCommentCreateModel = PostCommentCreateModel(
                commentId= uuid.uuid4().hex[:50],
                content= data.get("content"),
                postId= Post.objects.get(postId = postId),
                sequenceNum=0,
                timeCreated= timezone.now(),
                userId= User.objects.get(userId = userId),
                status= True
            )
        totalCommentOfPost = PostComment.objects.filter(postId = postId).count()
        try:
            with transaction.atomic():
                post = Post.objects.get(postId = postId)
                updatedPost = post
                updatedPost.totalComment = updatedPost.totalComment + 1
                updatedPostDict = updatedPost.__dict__
                if super().update(post, updatedPostDict):
                    postCommentSerializer = PostCommentSerializer()
                    check = False
                    if totalCommentOfPost >= 0:
                        postCommentCreateModel.sequenceNum = postCommentCreateModel.sequenceNum +1
                        check = postCommentSerializer.create(data= postCommentCreateModel)
                    else:
                        check = postCommentSerializer.create(data= postCommentCreateModel)
                    if check:
                        return True
                else:
                    transaction.rollback()
                    return False
        except Exception as e:
            print(e)
            return False
    

    def get_post_for_user(self, userId:str, page: int = 0):
        friends = UserFriend.objects.filter(friendId = userId)
        listFriendIds = []

        for item in friends.iterator():
            listFriendIds.append(item.userId.userId)
        
        begin = 0
        to = 3
        if page > 0:
            begin = page*3 +1
            to = begin + 3

        posts = Post.objects.filter(userId__in=listFriendIds).order_by("timeCreated").reverse()[begin:to]
        result = list()
        for item in posts:
            post = PostGeneralModel(
                        postId=item.postId,
                        description= item.description,
                        postImage=[],
                        timeCreated= item.timeCreated,
                        totalComment= item.totalComment,
                        totalLike= item.totalLike,
                        userId= item.userId.username,
                        postLike=[],
                        avatar=item.userId.avatar
                    )
            for image in PostImage.objects.filter(postId = post.postId):
                        image_dict = image.__dict__
                        image_dict.pop("_state")
                        post.postImage.append(image_dict)

            for like in PostLike.objects.filter(postId = post.postId):
                like_item = {
                    "username" : like.userId.username,
                    "fullname" : like.userId.fullname
                }
                post.postLike.append(like_item)
            
            result.append(post.__dict__)
        return result




        



    
    