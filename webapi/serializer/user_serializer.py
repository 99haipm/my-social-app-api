from typing import List
from pytz import timezone
from webapi.serializer.user_friend_serializer import UserFriendSerializer
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models
from django.db.models.base import Model
from rest_framework import fields, serializers
from webapi.models import User, Role, UserFriend
import uuid
from django.contrib.auth.hashers import make_password, check_password
import jwt
from django.db import transaction
from datetime import datetime
from django.utils import timezone
from django.db.models import Q

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('userId', 'username', 'password', 'fullname',
                  'email', 'avatar', 'status', 'roleId')

    def create(self, data: UserCreateModel):
        newUser = User(
            userId=uuid.uuid4().hex[:50],
            username=data.username,
            password=make_password(data.password),
            email=data.email,
            avatar=data.avatar,
            status="Active",
            fullname=data.fullname,
            roleId=Role.objects.get(id=1)
        )
        userDict = newUser.__dict__
        del userDict["_state"]

        return User.objects.create(**userDict)

    def login(self, data: UserLoginModel):

        currentUser = User.objects.get(username=data.username)
        if currentUser:
            check = check_password(data.password, currentUser.password)
            if check:
                payload = {
                    "role": currentUser.roleId.name,
                    "userId": currentUser.userId,
                    "username": currentUser.username,
                    "fullname": currentUser.fullname,
                    "issuer": "InstagramApp"
                }

                token = jwt.encode(payload=payload, key="my-secret-key-jwt")
                return token
        else:
            return None

    def fetch_user(self, token: str):
        userId = jwt.decode(token, key="my-secret-key-jwt",
                            algorithms="HS256").get('userId')
        if userId:
            currentUser = User.objects.get(userId=userId)
            print(currentUser)
            if currentUser:
                result = {
                    "username": currentUser.username,
                    "fullname": currentUser.fullname,
                    "role": currentUser.roleId.name,
                    "status": currentUser.status,
                    "avatar": currentUser.avatar,
                    "email": currentUser.email
                }
                return result
            else:
                return None
        else:
            return None

    def request_friend(self, userId: str, friendId: str):
        user = User.objects.get(userId=userId)
        friend = User.objects.get(userId=friendId)
        if user != None and friend != None:
            time = timezone.now()
            userFriend_1 = {
                "friendId": friend.userId,
                "username": friend.username,
                "status": "Requested",
                "timeCreated": time,
                "userId": user.userId
            }

            userFriend_2 = {
                "friendId": user.userId,
                "username": user.username,
                "status": "Waiting",
                "timeCreated": time,
                "userId": friend.userId
            }

            print(userFriend_1)
            try:
                with transaction.atomic():
                    userFriendSerializer_1 = UserFriendSerializer(
                        data=userFriend_1)
                    userFriendSerializer_2 = UserFriendSerializer(
                        data=userFriend_2)
                    if userFriendSerializer_1.is_valid() and userFriendSerializer_2.is_valid():
                        userFriendSerializer_1.save()
                        userFriendSerializer_2.save()
                        return True
                    else:
                        print(userFriendSerializer_1.error_messages)
                        print(userFriendSerializer_2.error_messages)
                        return False
            except Exception as e:
                print(e)
                return False
        else:
            return False

    def get_friend_request(self, token: str):
        userId = jwt.decode(token.split(" ").__getitem__(
            1), key="my-secret-key-jwt", algorithms="HS256").get('userId')
        user = User.objects.get(userId=userId)
        if user:
            friendRequests = UserFriend.objects.filter(
                userId=userId, status="Waiting")
            result = []
            for item in friendRequests.iterator():
                result.append({
                    "friendId": item.friendId,
                    "friendUsername": item.username,
                    "status": item.status,
                    "timeCreated": item.timeCreated
                })
            return result
        else:
            return []

    def accept_request(self, relationId: int):
        try:
            relation = UserFriend.objects.get(id=relationId, status="Waiting")

            if relation:
                request_relation = UserFriend.objects.get(
                    userId=relation.friendId)
                if request_relation:
                    instance_relation = relation
                    instance_relation.status = "Active"

                    instance_request_relation = request_relation
                    instance_request_relation.status = "Active"

                    instance_relation_dict = instance_relation.__dict__
                    instance_request_relation_dict = instance_request_relation.__dict__

                    del instance_relation_dict["_state"]

                    del instance_request_relation_dict["_state"]

                    try:
                        with transaction.atomic():
                            UserFriend.objects.filter(id=relationId, status="Waiting").update(
                                **instance_relation_dict)
                            UserFriend.objects.filter(userId=relation.friendId).update(
                                **instance_request_relation_dict)
                            return True
                    except Exception as e:
                        print(e)
                        return False
            else:
                return False
        except Exception as ex:
            print(ex)
            return False

    def get_recommend_friend(self, userId: str):
        friendOfUser = UserFriend.objects.filter(friendId=userId)
        result = []
        listUserFriendId = []
        for item in friendOfUser.iterator():
            listUserFriendId.append(item.userId.userId)
        recommends = User.objects.exclude(userId__in=listUserFriendId).exclude(userId = userId)
        for item in recommends.iterator():
            result.append({
                "username": item.username,
                "fullname": item.fullname,
                "role": item.roleId.name,
                "status": item.status,
                "avatar": item.avatar,
                "email": item.email
            })
        return result
