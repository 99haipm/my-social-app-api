
from typing import List
from webapi.serializer.post_image_serializer import PostImageSerializer
from webapi.view_model.postVMs import  PostGeneralModel, PostModel
from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models, transaction
from django.db.models.base import Model
from rest_framework import fields, serializers 
from webapi.models import Post, PostImage, PostLike, User,Role, UserFriend
import uuid 
from django.contrib.auth.hashers import make_password, check_password
import jwt


class PostLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostLike
        fields = ('id','userId','postId')
    
    def create(self, postInstance: Post, userInstance: User):
        dict = PostLike(userId = userInstance, postId = postInstance).__dict__
        dict.pop("_state")
        return super().create(validated_data= dict)
