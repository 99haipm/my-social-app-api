from webapi.view_model.userVMs import UserCreateModel, UserLoginModel
from django.db import models
from django.db.models.base import Model
from rest_framework import fields, serializers 
from webapi.models import User,Role, UserFriend
import uuid 
from django.contrib.auth.hashers import make_password, check_password
import jwt


class UserFriendSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserFriend
        fields = ('id', 'friendId', 'username','status','timeCreated','userId')
    