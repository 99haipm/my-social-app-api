


from django.http.request import HttpRequest


class UserCreateModel:
    def __init__(self, username: str, password: str, email: str, fullname: str ,avatar:str = ""):
        self.username = username
        self.password = password
        self.email = email
        self.fullname = fullname
        self.avatar = avatar


class UserLoginModel:
    def __init__(self, username:str, password: str):
        self.username = username
        self.password = password
    