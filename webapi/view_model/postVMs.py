


from typing import List
from webapi.models import Post, PostImage, User


class PostModel:

    def __init__(self,postId:str, description: str, totalLike: int, totalComment: int, timeCreated, userId : User, postImage = [],):
        self.postId = postId
        self.description = description
        self.totalLike = totalLike
        self.totalComment = totalComment
        self.timeCreated = timeCreated
        self.userId = userId
        self.postImage = postImage
    def __set_image_detail__(self, images: List[PostImage]):
        self.imageDetail = []
        for item in images:
            self.imageDetail.append(item.__dict__)


class PostGeneralModel:
    def __init__(self,postId:str, description: str, totalLike: int, totalComment: int, timeCreated, userId : str, postImage = [], postLike = [],avatar = ""):
        self.postId = postId
        self.description = description
        self.totalLike = totalLike
        self.totalComment = totalComment
        self.timeCreated = timeCreated
        self.userId = userId
        self.postImage = postImage
        self.postLike = postLike
        self.avatar = avatar


class PostCommentCreateModel:
    def __init__(self, commentId: str, sequenceNum: int, content: str, userId: User, timeCreated, postId: Post, status : bool):
        self.commentId = commentId
        self.sequenceNum = sequenceNum
        self.content = content
        self.userId = userId
        self.timeCreated = timeCreated
        self.postId = postId
        self.status = status
