from django.db import models

# Create your models here.
class User(models.Model):
    userId = models.TextField(primary_key=True, max_length=50)
    username = models.TextField(max_length=30, null= False, unique= True)
    password = models.TextField(max_length=70, null = False)
    fullname = models.TextField(max_length=50, null = False)
    email = models.TextField(max_length=50, null = False)
    avatar = models.TextField(max_length=200, null= True)
    status = models.TextField(max_length=20, null= False)
    roleId = models.ForeignKey("Role", on_delete= models.SET_NULL, null= True)

class Role(models.Model):
    name = models.TextField(max_length=50, null= False)
    description = models.TextField(max_length= max, null = True)
    status = models.TextField(max_length=20, null= False)


class UserFriend(models.Model):
    friendId = models.TextField(max_length=50, null = False)
    username = models.TextField(max_length=30, null = False)
    status = models.TextField(max_length=20, null= False)
    timeCreated = models.DateTimeField()
    userId = models.ForeignKey("User", on_delete= models.SET_NULL, null= True)


class Post(models.Model):
    postId = models.TextField(primary_key=True, max_length=50)
    description = models.TextField(max_length=max, null= True)
    totalLike = models.IntegerField(null= False, default= 0)
    totalComment = models.IntegerField(null = False, default=0)
    userId = models.ForeignKey("User",on_delete=models.SET_NULL, null=True)
    timeCreated = models.DateTimeField()


class PostImage(models.Model):
    imageId = models.TextField(primary_key=True, max_length= 50)
    imageUrl = models.TextField(max_length=200, null= False)
    status = models.TextField(max_length=20, null= False)
    postId = models.ForeignKey("Post", on_delete=models.SET_NULL, null= True)


class PostComment(models.Model):
    commentId = models.TextField(primary_key=True,max_length=50)
    sequenceNum = models.IntegerField(null = False)
    content = models.TextField(max_length= max)
    userId = models.TextField(max_length= 50, null= False)
    timeCreated = models.DateTimeField()  
    postId = models.ForeignKey("Post", on_delete=models.SET_NULL, null= True)
    status = models.BooleanField(default= False)
    userId = models.ForeignKey("User", on_delete=models.SET_NULL, null= True)

class PostLike(models.Model):
    postId = models.ForeignKey("Post", on_delete=models.SET_NULL, null= True)
    userId = models.ForeignKey("User",on_delete=models.SET_NULL, null=True)