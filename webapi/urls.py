from django.urls import include, path
from rest_framework import routers, urlpatterns
from . import views
from django.conf.urls import url 

from django.urls import path

from .endpoint import user_views, post_views

router = routers.DefaultRouter()


urlpatterns = [ 
    url('api/users/register', user_views.register),
    url('api/users/get-recommend', user_views.get_recommend),
    url('api/users/login', user_views.login),
    url('api/users/fetch-user', user_views.fetch_user),
    url('api/users/request-friend', user_views.request_friend),
    url('api/users/get-request-friend', user_views.get_friend_request),
    url('api/users/accept-request-friend', user_views.accept_request),
    url('api/posts/like-post', post_views.like_post),
    url('api/posts/comment-post', post_views.comment_post),
    url('api/posts/get-post', post_views.get_post_for_user),
    url('api/posts', post_views.post_api),
    
    # url(r'^api/tutorials/(?P<pk>[0-9]+)$', views.tutorial_detail),
    # url(r'^api/tutorials/published$', views.tutorial_list_published)
]