
from rest_framework.permissions import BasePermission, IsAuthenticated
import jwt
from rest_framework.request import HttpRequest, Request

class IsAuth(IsAuthenticated):
    def has_permission(self, request : HttpRequest, view):
        token = request.headers.get('Authorization')
        issuers = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('issuer')
        if issuers == "InstagramApp":
            return True
        else:
            return False


class IsUser(BasePermission):
    def has_permission(self, request: HttpRequest, view):
        token = request.headers.get('Authorization')
        role = jwt.decode(token.split(" ").__getitem__(1), key="my-secret-key-jwt",algorithms="HS256").get('role')
        if role == "user":
            return True
        else:
            return False